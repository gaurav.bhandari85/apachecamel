package org.camel.mongo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalledClass {
    Logger log = LoggerFactory.getLogger(CalledClass.class);
    void hello()
    {
        System.out.println("Open the book");
    }
}
