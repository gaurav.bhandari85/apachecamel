package org.camel.mongo;

import org.apache.camel.CamelContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static java.lang.Thread.*;

public class MainApp {
    public static void main(String[] args)
    {
        MainApp app = new MainApp();
        app.process();
    }

    private void process() {
        ApplicationContext appctx = new ClassPathXmlApplicationContext("beans.xml");
        CamelContext ctx = (CamelContext) appctx.getBean("camel");
        ctx.start();
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
