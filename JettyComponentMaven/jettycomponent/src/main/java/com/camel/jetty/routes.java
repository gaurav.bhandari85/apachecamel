package com.camel.jetty;

import org.apache.camel.builder.RouteBuilder;

public class routes extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("jetty:http://localhost:8000")
                .to("class:com.camel.jetty.JettyRoute?method=hell");

    }
}
