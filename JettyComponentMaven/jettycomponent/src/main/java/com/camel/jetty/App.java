package com.camel.jetty;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        App a = new App();
        try {
            a.hello();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    void hello() throws Exception {
        CamelContext ctx = new DefaultCamelContext();
        ctx.addRoutes(new routes());
        ctx.start();
    }
}
