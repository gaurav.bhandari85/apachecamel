package routes;

import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class FileCreateRoute extends RouteBuilder {
    Logger log = LoggerFactory.getLogger(FileCreateRoute.class);
    @Override
    public void configure() throws Exception {

        from("stream:in?promptMessage=Enter something:").to("file:outbox?filename=mydata-${date:now:yyyyMMddHHmmssSSS}.txt");
        System.out.println("Starting");
        //from("direct:start").to("class://org.camel.filecomponent.CalledClass");
        System.out.println("Ended");

    }
}
