package org.camel.filecomponent;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import routes.FileCreateRoute;

public class MainClass {

    public static void main (String[] args)
    {
        MainClass app = new MainClass();
        app.process();
    }

    private void process() {
        CamelContext ctx = new DefaultCamelContext();
        try {
            ctx.addRoutes(new FileCreateRoute());

            ctx.start();
            ProducerTemplate template = ctx.createProducerTemplate();

            template.sendBody("direct:start", "This is a test message");
            Thread.sleep(100000);
            ctx.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
