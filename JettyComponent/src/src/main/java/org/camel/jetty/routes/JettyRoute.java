package org.camel.jetty.routes;

import org.apache.camel.builder.RouteBuilder;

public class JettyRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        from("jetty:http://localhost:9777/hello?continuationTimeout=0")
                .log("Connection Received")
                .setBody(simple("Hello World"))
                .to("class:org.camel.jetty.CalledClass?method=hello")

                ;
    }
}
