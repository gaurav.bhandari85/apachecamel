package org.camel.jetty;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.camel.jetty.routes.JettyRoute;

public class MainClass {
    public static void main(String[] args)
    {
        MainClass app = new MainClass();
        app.process();
    }

    private void process() {
        CamelContext ctx = new DefaultCamelContext();
        try {
            //ctx.addComponent("jetty");
            ctx.addRoutes(new JettyRoute());
            ctx.start();
            ctx.setTracing(true);
            Thread.sleep(10000);

        } catch (Exception e) {
            e.printStackTrace();
        }
        ctx.stop();

    }
}
